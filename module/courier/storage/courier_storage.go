package storage

import (
	"context"
	"encoding/json"
	"github.com/redis/go-redis/v9"
	"gitlab.com/konfka/go-courier/module/courier/models"
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name CourierStorager
type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // Сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // Получить курьера по ключу courier
}

type CourierStorage struct {
	storage *redis.Client
}

func NewCourierStorage(storage *redis.Client) CourierStorager {
	return &CourierStorage{storage: storage}
}

func (c CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	var err error
	var data []byte

	// сериализуем курьера в json
	data, err = json.Marshal(courier)
	if err != nil {
		return err
	}
	// сохраняем курьера в json redis по ключу couriers

	return c.storage.Set(ctx, "couriers", data, 0).Err()
}

func (c CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	data, err := c.storage.Get(ctx, "couriers").Bytes()
	if err != nil {
		return nil, err
	}

	courier := models.Courier{}

	err = json.Unmarshal(data, &courier)
	if err != nil {
		return nil, err
	}

	return &courier, nil
}
