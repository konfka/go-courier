package service

import (
	"context"
	"github.com/redis/go-redis/v9"
	"gitlab.com/konfka/go-courier/geo"
	"gitlab.com/konfka/go-courier/module/courier/models"
	"gitlab.com/konfka/go-courier/module/courier/storage"
	"math"
)

// Направления движения курьера
const (
	DirectionUp    = 0
	DirectionDown  = 1
	DirectionLeft  = 2
	DirectionRight = 3
)

const (
	DefaultCourierLat = 59.9311
	DefaultCourierLng = 30.3609
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name Courierer
type Courierer interface {
	GetCourier(ctx context.Context) (*models.Courier, error)
	MoveCourier(courier models.Courier, direction, zoom int) error
	ScoreUpdate(courier models.Courier, scores int) error
}

type CourierService struct {
	courierStorage storage.CourierStorager
	allowedZone    geo.PolygonChecker
	disabledZones  []geo.PolygonChecker
}

func NewCourierService(courierStorage storage.CourierStorager, allowedZone geo.PolygonChecker, disabledZones []geo.PolygonChecker) Courierer {
	return &CourierService{courierStorage: courierStorage, allowedZone: allowedZone, disabledZones: disabledZones}
}

func (c *CourierService) GetCourier(ctx context.Context) (*models.Courier, error) {
	// получаем курьера из хранилища используя метод GetOne из storage/courier.go
	courier, err := c.courierStorage.GetOne(ctx)
	if err == redis.Nil {
		courier = new(models.Courier)
		courier.Location.Lat = DefaultCourierLat
		courier.Location.Lng = DefaultCourierLng

		err = nil
	}

	if err != nil {
		return nil, err
	}

	point := geo.Point{courier.Location.Lat, courier.Location.Lng}

	// проверяем, что курьер находится в разрешенной зоне
	isAllowed := geo.CheckPointIsAllowed(point, c.allowedZone, c.disabledZones)
	// если нет, то перемещаем его в случайную точку в разрешенной зоне
	// сохраняем новые координаты курьера
	if !isAllowed {
		point = geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
		courier.Location.Lat, courier.Location.Lng = point.Lat, point.Lng
	}

	err = c.courierStorage.Save(ctx, *courier)
	if err != nil {
		return nil, err
	}

	return courier, nil
}

// MoveCourier : direction - направление движения курьера, zoom - зум карты
func (c *CourierService) MoveCourier(courier models.Courier, direction, zoom int) error {
	// точность перемещения зависит от зума карты использовать формулу 0.001 / 2^(zoom - 14)
	accuracy := 0.001 / math.Pow(2, float64(zoom-14))
	// 14 - это максимальный зум карты
	switch direction {
	case DirectionUp:
		courier.Location.Lat += accuracy
	case DirectionDown:
		courier.Location.Lat -= accuracy
	case DirectionLeft:
		courier.Location.Lng -= accuracy
	case DirectionRight:
		courier.Location.Lng += accuracy
	}
	// далее нужно проверить, что курьер не вышел за границы зоны
	// если вышел, то нужно переместить его в случайную точку внутри зоны
	point := geo.Point{
		Lat: courier.Location.Lat,
		Lng: courier.Location.Lng,
	}
	if !geo.CheckPointIsAllowed(point, c.allowedZone, c.disabledZones) {
		point = geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
		courier.Location.Lat, courier.Location.Lng = point.Lat, point.Lng
	}
	// далее сохранить изменения в хранилище

	err := c.courierStorage.Save(context.Background(), courier)

	if err != nil {
		return err
	}
	return nil
}

func (c *CourierService) ScoreUpdate(courier models.Courier, scores int) error {
	courier.Score += scores
	err := c.courierStorage.Save(context.Background(), courier)
	if err != nil {
		return err
	}

	return nil
}
