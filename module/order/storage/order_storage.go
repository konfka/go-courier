package storage

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/redis/go-redis/v9"
	"gitlab.com/konfka/go-courier/module/order/models"
	"strconv"
	"time"
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name OrderStorager
type OrderStorager interface {
	Save(ctx context.Context, order models.Order, maxAge time.Duration) error                       // Сохранить заказ с временем жизни
	GetByID(ctx context.Context, orderID int) (*models.Order, error)                                // Получить заказ по id
	GenerateUniqueID(ctx context.Context) (int64, error)                                            // Сгенерировать уникальный id
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // Получить заказы в радиусе от точки
	GetCount(ctx context.Context) (int, error)                                                      // Получить количество заказов
	RemoveOldOrders(ctx context.Context, maxAge time.Duration) error                                // Удалить старые заказы по истечению времени maxAge
}

type OrderStorage struct {
	storage *redis.Client
}

func NewOrderStorage(storage *redis.Client) OrderStorager {
	return &OrderStorage{storage: storage}
}

func (o *OrderStorage) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {
	// save with geo redis
	return o.saveOrderWithGeo(ctx, order, maxAge)
}

func (o *OrderStorage) RemoveOldOrders(ctx context.Context, maxAge time.Duration) error {
	// получить ID всех старых ордеров, удаляя их с помощью метода ZRangeByScore
	// старые ордеры это те, которые были созданы две минуты назад
	// и более
	t := &redis.ZRangeBy{
		Max: "+inf",
		Min: strconv.FormatInt(time.Now().Add(-maxAge).UnixNano(), 10),
	}
	// Проверить количество старых ордеров
	slice, err := o.storage.ZRangeByScoreWithScores(ctx, "orders", t).Result()
	if err != nil {
		return err
	}
	if len(slice) == 0 {
		return nil
	}

	//ord := fmt.Sprintf("%v", slice[len(slice)-1].Score)
	// удалить старые ордеры из redis используя метод ZRemRangeByScore, где ключ "orders" min "-inf" max "(время создания старого ордера)"
	// удалять ордера по ключу не нужно, они будут удалены автоматически по истечению времени жизни
	err = o.storage.ZRemRangeByScore(ctx, "orders", "-inf", strconv.FormatInt(time.Now().Add(-maxAge).UnixNano(), 10)).Err()
	if err != nil {
		return err
	}

	return nil
}

func (o *OrderStorage) GetByID(ctx context.Context, orderID int) (*models.Order, error) {
	var err error
	var data []byte
	var order models.Order
	// получаем ордер из redis по ключу order:ID
	res, err := o.storage.Get(ctx, fmt.Sprintf("order:%d", orderID)).Result()
	// проверяем что ордер не найден исключение redis.Nil, в этом случае возвращаем nil, nil
	if err == redis.Nil {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	// десериализуем ордер из json
	data = []byte(res)
	err = json.Unmarshal(data, &order)
	if err != nil {
		return nil, err
	}
	return &order, nil
}

func (o *OrderStorage) saveOrderWithGeo(ctx context.Context, order models.Order, maxAge time.Duration) error {
	var err error
	var data []byte

	// сериализуем ордер в json
	data, err = json.Marshal(order)
	if err != nil {
		return err
	}
	// сохраняем ордер в json redis по ключу order:ID с временем жизни maxAge
	err = o.storage.Set(ctx, fmt.Sprintf("order:%d", order.ID), data, maxAge).Err()
	if err != nil {
		return err
	}
	// добавляем ордер в гео индекс используя метод GeoAdd где Name - это ключ ордера, а Longitude и Latitude - координаты
	err = o.storage.GeoAdd(ctx, "order_geo", &redis.GeoLocation{
		Name:      fmt.Sprintf("order:%d", order.ID),
		Longitude: order.Lng,
		Latitude:  order.Lat,
	}).Err()
	if err != nil {
		return err
	}

	// zset сохраняем ордер для получения количества заказов со сложностью O(1)
	// Score - время создания ордера
	err = o.storage.ZAdd(ctx, "orders", redis.Z{
		Score:  float64(order.CreatedAt.UnixNano()),
		Member: fmt.Sprintf("%d", order.ID),
	}).Err()
	if err != nil {

		return err
	}

	return nil
}

func (o *OrderStorage) GetCount(ctx context.Context) (int, error) {
	// получить количество ордеров в упорядоченном множестве используя метод ZCard
	count, err := o.storage.ZCard(ctx, "orders").Result()
	if err != nil {
		return 0, err
	}
	return int(count), nil
}

func (o *OrderStorage) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	var err error
	var orders []models.Order
	var ordersLocation []redis.GeoLocation

	// используем метод getOrdersByRadius для получения ID заказов в радиусе
	ordersLocation, err = o.getOrdersByRadius(ctx, lng, lat, radius, unit)
	// обратите внимание, что в случае отсутствия заказов в радиусе
	// метод getOrdersByRadius должен вернуть nil, nil (при ошибке redis.Nil)
	if err == redis.Nil {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	orders = make([]models.Order, 0, len(ordersLocation))
	// проходим по списку ID заказов и получаем данные о заказе
	for _, v := range ordersLocation {
		data, err := o.storage.Get(ctx, v.Name).Bytes()
		if err != nil {
			if err == redis.Nil {
				return nil, nil
			}
			return nil, err
		}
		order := models.Order{}
		err = json.Unmarshal(data, &order)
		if err != nil {
			return nil, err
		}
		orders = append(orders, order)
	}
	// получаем данные о заказе по ID из redis по ключу order:ID

	return orders, nil
}

func (o *OrderStorage) getOrdersByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]redis.GeoLocation, error) {
	// в данном методе мы получаем список ордеров в радиусе от точки
	// возвращаем список ордеров с координатами и расстоянием до точки
	query := &redis.GeoRadiusQuery{
		Radius:      radius,
		Unit:        unit,
		WithCoord:   true,
		WithDist:    true,
		WithGeoHash: true,
	}
	locations, err := o.storage.GeoRadius(ctx, "orders_geo", lng, lat, query).Result()
	if err != nil {
		return nil, err
	}
	return locations, nil
}

func (o *OrderStorage) GenerateUniqueID(ctx context.Context) (int64, error) {
	var err error
	var id int64

	// генерируем уникальный ID для ордера
	// используем для этого redis incr по ключу order:id
	id, err = o.storage.Incr(ctx, "order:id").Result()
	if err != nil {
		if err == redis.Nil {
			return 0, nil
		}
		return 0, err
	}
	return id, nil
}
