package models

import (
	cm "gitlab.com/konfka/go-courier/module/courier/models"
	om "gitlab.com/konfka/go-courier/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
