package service

import (
	"context"
	"github.com/stretchr/testify/mock"
	cmodels "gitlab.com/konfka/go-courier/module/courier/models"
	"gitlab.com/konfka/go-courier/module/courier/service/mocks"
	"gitlab.com/konfka/go-courier/module/courierfacade/models"
	omodels "gitlab.com/konfka/go-courier/module/order/models"
	"gitlab.com/konfka/go-courier/module/order/service"
	"reflect"
	"testing"
)

func TestCourierFacade_GetStatus(t *testing.T) {
	type fields struct {
		courierService *mocks.Courierer
		orderService   *service.MockOrderer
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.CourierStatus
	}{
		{
			name: "test Get_Status",
			fields: fields{
				courierService: mocks.NewCourierer(t),
				orderService:   service.NewMockOrderer(t),
			},
			args: args{
				context.Background(),
			},
			want: models.CourierStatus{
				Courier: cmodels.Courier{},
				Orders:  []omodels.Order{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.courierService.On("GetCourier", tt.args.ctx).Return(&cmodels.Courier{}, nil)
			tt.fields.orderService.On("GetByRadius", tt.args.ctx, 0.0, 0.0, mock.Anything, "m").Return([]omodels.Order{}, nil)
			c := CourierFacade{
				courierService: tt.fields.courierService,
				orderService:   tt.fields.orderService,
			}
			if got := c.GetStatus(tt.args.ctx); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCourierFacade_MoveCourier(t *testing.T) {
	type fields struct {
		courierService *mocks.Courierer
		orderService   *service.MockOrderer
	}
	type args struct {
		ctx       context.Context
		direction int
		zoom      int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "test MoveCourier",
			fields: fields{
				courierService: mocks.NewCourierer(t),
				orderService:   service.NewMockOrderer(t),
			},
			args: args{
				ctx:       context.Background(),
				direction: 0,
				zoom:      0,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.courierService.On("GetCourier", tt.args.ctx).Return(&cmodels.Courier{}, nil)
			tt.fields.courierService.On("MoveCourier", cmodels.Courier{}, 0, 0).Return(nil)
			c := CourierFacade{
				courierService: tt.fields.courierService,
				orderService:   tt.fields.orderService,
			}
			c.MoveCourier(tt.args.ctx, tt.args.direction, tt.args.zoom)
		})
	}
}
