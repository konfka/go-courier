package service

import (
	"context"
	cservice "gitlab.com/konfka/go-courier/module/courier/service"
	cfm "gitlab.com/konfka/go-courier/module/courierfacade/models"
	oservice "gitlab.com/konfka/go-courier/module/order/service"
	"log"
)

const (
	CourierVisibilityRadius = 2500 // 2.5km
	CourierDeliveryRadius   = 5
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name CourierFacer
type CourierFacer interface {
	MoveCourier(ctx context.Context, direction, zoom int) // Отвечает за движение курьера по карте direction - направление движения, zoom - уровень зума
	GetStatus(ctx context.Context) cfm.CourierStatus      // Отвечает за получение статуса курьера и заказов вокруг него
}

// CourierFacade фасад для курьера и заказов вокруг него (для фронта)
type CourierFacade struct {
	courierService cservice.Courierer
	orderService   oservice.Orderer
}

func NewCourierFacade(courierService cservice.Courierer, orderService oservice.Orderer) CourierFacer {
	return &CourierFacade{courierService: courierService, orderService: orderService}
}

func (c CourierFacade) MoveCourier(ctx context.Context, direction, zoom int) {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Fatalln(err)
	}

	err = c.courierService.MoveCourier(*courier, direction, zoom)
	if err != nil {
		log.Fatalln(err)
	}
}

func (c CourierFacade) GetStatus(ctx context.Context) cfm.CourierStatus {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Fatalln(err)
	}

	orders, err := c.orderService.GetByRadius(ctx, courier.Location.Lng, courier.Location.Lat, CourierVisibilityRadius, "m")
	if err != nil {
		log.Fatalln(err)
	}

	ordersDelivery, err := c.orderService.GetByRadius(ctx, courier.Location.Lng, courier.Location.Lat, CourierDeliveryRadius, "m")
	if err != nil {
		log.Fatalln(err)
	}

	for _, ord := range ordersDelivery {
		err = c.orderService.Delivery(ctx, ord)
		if err != nil {
			log.Fatalln(err)
		}

		err = c.courierService.ScoreUpdate(*courier, 1)
		if err != nil {
			log.Fatalln(err)
		}
	}

	return cfm.CourierStatus{
		Courier: *courier,
		Orders:  orders,
	}
}
